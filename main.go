package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/user"
	"time"
)


type CryptoADA struct {
	Title map[string]float64 `json:"cardano"`
}

type CryptoXMR struct {
	Title map[string]float64 `json:"monero"`
}

const url_ada string = "https://api.coingecko.com/api/v3/simple/price?ids=cardano&vs_currencies=usd"
const url_xmr string = "https://api.coingecko.com/api/v3/simple/price?ids=monero&vs_currencies=usd"
const ada string = "ADA"
const xmr string = "XMR"

var currentTime string = time.Now().Format("2006-01-02")

func main() {

	dir := "/Documents/org/"
	usr, err := user.Current()
	isError(err)
	home := usr.HomeDir

	var ada_file string = home + dir + "ada-price.beancount"
	var xmr_file string = home + dir + "xmr-price.beancount"

	data_ada := getADA(url_ada)
	data_xmr := getXMR(url_xmr)

	str_ada := formatStringADA(ada, currentTime, data_ada)
	str_xmr := formatStringXMR(xmr, currentTime, data_xmr)

	writeCall(str_ada, ada_file)
	writeCall(str_xmr, xmr_file)
}

func formatStringADA(ticker string, currentTime string, data CryptoADA) string {
    quote := data.Title
	str := fmt.Sprintf("%v price %v %.8v EUR\n", currentTime, ticker, quote["usd"])
	return str
}

func formatStringXMR(ticker string, currentTime string, data CryptoXMR) string {
    quote := data.Title
	str := fmt.Sprintf("%v price %v %.8v EUR\n", currentTime, ticker, quote["usd"])
	return str
}

func writeCall(content string, loc string) int {
	file, err := os.OpenFile(loc, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	isError(err)

	length, err := io.WriteString(file, content)
	isError(err)
	defer file.Close()

	return length
}

func getADA(url_ada string) CryptoADA {
	resp, err := http.Get(url_ada)
	isError(err)

	body, err := ioutil.ReadAll(resp.Body)
	isError(err)

	defer resp.Body.Close() // close the connection
    
    // Unmarshal json into struct
	var data_ada CryptoADA
	err = json.Unmarshal(body, &data_ada)
	if err != nil {
		log.Fatal(err)
	}
	return data_ada
}

func getXMR(url_xmr string) CryptoXMR {
	resp, err := http.Get(url_xmr)
	isError(err)

	body, err := ioutil.ReadAll(resp.Body)
	isError(err)

	defer resp.Body.Close() // close the connection
    
    // Unmarshal json into struct
	var data_xmr CryptoXMR
	err = json.Unmarshal(body, &data_xmr)
	if err != nil {
		log.Fatal(err)
	}
	return data_xmr
}

func isError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
